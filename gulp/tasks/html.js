var gulp = require('gulp');
var $ = require('gulp-load-plugins')({ camelize: true });
var config = require('../config').html;

gulp.task('html', function() {
    return gulp.src(config.src)
    .pipe($.fileInclude())
    .pipe(gulp.dest(config.dest));
});
