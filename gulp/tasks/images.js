var gulp = require('gulp');
var $ = require('gulp-load-plugins')({ camelize: true });
var config = require('../config').images;

gulp.task('images', function() {
    return gulp.src(config.build.src)
    .pipe($.changed(config.build.dest))
    // .pipe($.imagemin(config.imagemin))
    .pipe(gulp.dest(config.build.dest));
});
