var gulp = require('gulp');
var $ = require('gulp-load-plugins')({ camelize: true });
var config = require('../config').scripts;

gulp.task('scripts', function() {
    return gulp.src(config.src)
    .pipe($.changed(config.dest))
    .pipe(gulp.dest(config.dest));
});
