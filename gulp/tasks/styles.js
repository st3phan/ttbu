var gulp = require('gulp');
var $ = require('gulp-load-plugins')({ camelize: true });
var config = require('../config').styles;
var processors = [
	require('autoprefixer')(config.autoprefixer),
	require('css-mqpacker')(),
	require('cssnano')()
];

gulp.task('styles', function() {
    return gulp.src(config.build.src)
    .pipe($.sourcemaps.init())
    .pipe($.sass(config.libsass).on('error', $.sass.logError))
	.pipe($.cssStr2base64())
    .pipe($.postcss(processors))
    .pipe($.rename(config.rename))
    .pipe($.sourcemaps.write('.'))
    .pipe(gulp.dest(config.build.dest));
});
