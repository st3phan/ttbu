var gulp= require('gulp');
var config = require('../config').watch;

gulp.task('watch', ['browsersync'], function() {
    gulp.watch(config.styles, ['styles']);
    gulp.watch(config.scripts, ['scripts']);
    gulp.watch(config.images, ['images']);
    gulp.watch(config.html, ['html']);
});
