var src = './src';
var build = './build';
var dist = '/.dist';

module.exports = {
    browsersync: {
        server: {
            baseDir: [
                build
            ],
        },
        files: [build+'/**', '!'+build+'/**.map'],
        notify: false,
        open: false,
        port: 3000,
        ghostMode: false,
        watchOptions: {
            debounceDelay: 2000
        }
    },
    images: {
        build: {
            src: src+'/img/**/*(*.svg|*.png|*.jpg|*.jpeg|*.gif)',
            dest: build+'/img/'
        },
        dist: {
            src: [
                dist+'/img/**/*(*.svg|*.png|*.jpg|*.jpeg|*.gif)'
            ],
            imagemin: {
                optimizationLevel: 7,
                progressive: true,
                interlaced: true
            },
            dest: dist
        }
    },
    styles: {
        build: {
            src: [
                src+'/scss/*.scss',
                '!'+src+'/scss/_*.scss'
            ],
            dest: build+'/css'
        },
        dist: {

        },
        compiler: 'libsass', // 'libsass' or 'ruby-sass'
        autoprefixer: {
            browsers: ['> 3%', 'last 2 versions', 'ie 9', 'ios 6', 'android 4']
        },
        rename: {
            suffix: '.min'
        },
        minify: {
            keepSpecialComments: 1,
            roundingPrecision: 3
        },
        libsass: { // Requires the libsass implementation of Sass
            precision: 6
        }
    },
    html: {
        src: [src+'/html/**/*.html', '!'+src+'/html/includes/**/*.html'],
        layout: src+'/html/layout.html',
        dest: build
    },
    scripts: {
        src: src+'/js/*.js',
        dest: build+'/js'
    },
    clean: {
        src: build+'/**/*'
    },
    watch: {
        styles: src+'/scss/**/*.scss',
        images: src+'/img/src/**/*(*.svg|*.png|*.jpg|*.jpeg|*.gif)',
        html: src+'/html/**/*.html',
        scripts: src+'/js/*.js'
    }
};
